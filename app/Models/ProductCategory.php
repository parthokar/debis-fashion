<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;

class ProductCategory extends Model
{

    protected $fillable = [
        'product_category',
        'image',
        'created_by',
        'status',
    ];

    public function productCount($catid){
        return Product::where('category_id',$catid)->count();
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_by = Auth::id();
        });
    }
    use HasFactory;
}
