<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\OrderItem;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
        'invoice_no',
        'customer_id',
        'name',
        'email',
        'mobile',
        'address',
    ];

    public function orderItem(){
       return $this->hasMany(OrderItem::class,'order_id');
    }
}
