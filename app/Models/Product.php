<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\ProductCategory;
use App\Models\ProductAttribute;
use App\Models\User;

class Product extends Model
{

    protected $fillable = [
        'category_id',
        'product_code',
        'product_title',
        'product_price',
        'product_image',
        'product_description',
        'created_by',
        'status',
    ];
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_by = Auth::id();
        });
    }

    public function category(){
        return $this->belongsTo(ProductCategory::class);
    }

    public function created_user(){
        return $this->belongsTo(User::class,'created_by');
    }

    public function attribute(){
        return $this->hasMany(ProductAttribute::class);
    }


    use HasFactory;
}
