<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Attribute;

class ProductAttribute extends Model
{

    protected $fillable = [
        'product_id',
        'att_type_id',
    ];
    use HasFactory;

    public function attributeDetails(){
        return $this->belongsTo(Attribute::class,'att_type_id');
    }
}
