<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Cart extends Model
{
    use HasFactory;
    protected $fillable = [
        'product_id',
        'attribute_id',
        'customer_id',
        'quantity',
        'total_price',
    ];

    public function products(){
        return $this->belongsTo(Product::class,'product_id');
    }
}
