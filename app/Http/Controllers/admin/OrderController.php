<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use DataTables;

class OrderController extends Controller
{

      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Order::orderBy('id','desc')->get();
            return Datatables::of($data)
                    ->addIndexColumn()

                    ->addColumn('created', function($row){
                        return date('Y-m-d h:i a',strtotime($row->created_at));
                     })

                    ->addColumn('action', function($row){
                            return '<a href="' . route('order.edit', $row->id) .'" class="edit btn btn-primary btn-sm">View More</a>';
                     })
                    ->rawColumns(['created','action'])
                    ->make(true);
        }
        return view('dashboard.admin.order.index');
    }

    public function edit($id){

      $data = Order::find($id)->first();

      $dataItem = OrderItem::where('order_id',$id)
      ->leftjoin('products','products.id','=','order_items.product_id')
      ->leftjoin('attributes','attributes.id','=','order_items.attribute_id')
      ->get();

      $total = OrderItem::where('order_id',$id)->sum('total_price');

      return view('dashboard.admin.order.details',compact('data','dataItem','total'));

    }

}
