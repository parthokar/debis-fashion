<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProductCategory;
use DataTables;

class ProductCategoryController extends Controller
{

      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = ProductCategory::orderBy('id','desc')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status', function($row){
                         if($row->status==1){
                           return 'Active';
                         }else{
                            return 'In active';
                         }
                    })
                    ->addColumn('image', function($row){
                        return '<img src="'.asset('dashboard/category/'.$row->image).'" width="30%" height="30%" class="img-fluid">';
                     })
                    ->addColumn('action', function($row){
                        return '<a href="' . route('category.edit', $row->id) .'" class="edit btn btn-primary btn-sm">Edit</a>';
                     })
                    ->rawColumns(['action','image'])
                    ->make(true);
        }
        return view('dashboard.admin.product.category.index');
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('dashboard.admin.product.category.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $request->validate([
            'product_category' => 'required|unique:product_categories,product_category',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

          // Get the file from the request
          $image = $request->file('image');

          // Generate a unique name for the file to avoid conflicts
          $filename = time().'.'.$image->getClientOriginalExtension();

          // Save the image to the public folder
          $image->move('dashboard/category/', $filename);

          ProductCategory::create([
                'product_category'=>$request->product_category,
                'image'=>  $filename,
                'status'=>$request->status,
          ]);

          return redirect()->route('category.index')->with('success','Category has been created successfully.');
      }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\CourseCategory  $company
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {

        $category=ProductCategory::find($id);
        return view('dashboard.admin.product.category.edit',compact('category'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\company  $company
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request,$id)
    {
        $category=ProductCategory::find($id);
        $request->validate([
            'product_category' => 'required|unique:product_categories,product_category,'.$category->id,
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->hasFile('image')) {
            // Get the file from the request
      $image = $request->file('image');

      // Generate a unique name for the file to avoid conflicts
      $filename = time().'.'.$image->getClientOriginalExtension();

      // Save the image to the public folder
      $image->move('dashboard/category/', $filename);
      }
       else
       {
            $filename = $category->image;
       }

      ProductCategory::where('id',$id)->update([
        'product_category'=>$request->product_category,
        'image'=>  $filename,
        'status'=>$request->status,
      ]);

        return redirect()->route('category.index')->with('success','Category Has Been updated successfully');
    }

}
