<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slider;
use DataTables;

class SliderController extends Controller
{

      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Slider::orderBy('id','desc')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status', function($row){
                         if($row->status==1){
                           return 'Active';
                         }else{
                            return 'In active';
                         }
                    })
                    ->addColumn('image', function($row){
                        return '<img src="'.asset('dashboard/slider/'.$row->image).'" width="30%" height="30%" class="img-fluid">';
                     })
                    ->addColumn('action', function($row){
                        return '<a href="' . route('slider.edit', $row->id) .'" class="edit btn btn-primary btn-sm">Edit</a>';
                     })
                    ->rawColumns(['action','image'])
                    ->make(true);
        }
        return view('dashboard.admin.slider.index');
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('dashboard.admin.slider.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $request->validate([
            'slider_title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

          // Get the file from the request
          $image = $request->file('image');

          // Generate a unique name for the file to avoid conflicts
          $filename = time().'.'.$image->getClientOriginalExtension();

          // Save the image to the public folder
          $image->move('dashboard/slider/', $filename);

          Slider::create([
                'slider_title'=>$request->slider_title,
                'image'=>  $filename,
                'status'=>$request->status,
          ]);

          return redirect()->route('slider.index')->with('success','Slider has been created successfully.');
      }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\CourseCategory  $company
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {

        $category=Slider::find($id);
        return view('dashboard.admin.slider.edit',compact('category'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\company  $company
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request,$id)
    {
        $category=Slider::find($id);
        $request->validate([
            'slider_title' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->hasFile('image')) {
            // Get the file from the request
      $image = $request->file('image');

      // Generate a unique name for the file to avoid conflicts
      $filename = time().'.'.$image->getClientOriginalExtension();

      // Save the image to the public folder
      $image->move('dashboard/slider/', $filename);
      }
       else
       {
            $filename = $category->image;
       }

      Slider::where('id',$id)->update([
        'slider_title'=>$request->slider_title,
        'image'=>  $filename,
        'status'=>$request->status,
      ]);

        return redirect()->route('slider.index')->with('success','Slider Has Been updated successfully');
    }

}
