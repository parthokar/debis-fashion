<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductAttribute;
use App\Models\Attribute;
use DataTables;

class ProductController extends Controller
{

      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Product::with('category','created_user')->orderBy('id','desc')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status', function($row){
                         if($row->status==1){
                           return 'Active';
                         }else{
                            return 'In active';
                         }
                    })
                    ->addColumn('category', function($row){
                       return $row->category->product_category;
                    })
                    ->addColumn('image', function($row){
                        return '<img src="'.asset('dashboard/product/'.$row->product_image).'" width="30%" height="30%" class="img-fluid">';
                     })
                    ->addColumn('created', function($row){
                        return $row->created_user->name;
                     })
                    ->addColumn('action', function($row){
                        if($row->created_by==auth()->user()->id){
                            return '<a href="' . route('product.edit', $row->id) .'" class="edit btn btn-primary btn-sm">Edit</a>';
                        }
                     })
                    ->rawColumns(['status','category','image','created','action'])
                    ->make(true);
        }
        return view('dashboard.admin.product.index');
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $category = ProductCategory::where('status',1)->get();
        $att_type = Attribute::select('id','type')->get();
        return view('dashboard.admin.product.create',compact('category','att_type'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'product_title' => 'required',
            'product_price' => 'required|numeric',
            'product_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            'status' => 'required',
        ]);

        // Get the file from the request
        $image = $request->file('product_image');

        // Generate a unique name for the file to avoid conflicts
        $filename = time().'.'.$image->getClientOriginalExtension();

        // Save the image to the public folder
        $image->move('dashboard/product/', $filename);

       $data = Product::create([
            'category_id'=>$request->category_id,
            'product_title'=>$request->product_title,
            'product_price'=>$request->product_price,
            'regular_price'=>$request->regular_price,
            'product_description'=>$request->product_description,
            'product_image'=>$filename,
            'status'=>$request->status,
        ]);

        if(isset($request->attribute)){
           for($i=0;$i<count($request->attribute);$i++){
                $att = new ProductAttribute;
                $att->product_id = $data->id;
                $att->att_type_id = $request->attribute[$i];
                $att->save();
           }
        }

        return redirect()->route('product.index')->with('success','Product has been created successfully.');
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\CourseCategory  $company
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $data['product']    = Product::find($id);
        $data['category']   = ProductCategory::where('status',1)->get();
        $data['att_type']   = Attribute::select('id','type')->get();
        return view('dashboard.admin.product.edit',compact('data'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\company  $company
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request,$id)
    {

        $product=Product::find($id);

        $request->validate([
            'category_id' => 'required',
            'product_title' => 'required',
            'product_price' => 'required|numeric',
            'product_image' => 'image|mimes:jpeg,png,jpg,gif,svg,webp|max:2048',
            'status' => 'required',
        ]);

        if ($request->hasFile('product_image')) {
                  // Get the file from the request
            $image = $request->file('product_image');

            // Generate a unique name for the file to avoid conflicts
            $filename = time().'.'.$image->getClientOriginalExtension();

            // Save the image to the public folder
            $image->move('dashboard/product/', $filename);
        }else{
            $filename = $product->product_image;
        }

        Product::where('id',$id)->update([
            'category_id'=>$request->category_id,
            'product_title'=>$request->product_title,
            'product_price'=>$request->product_price,
            'regular_price'=>$request->regular_price,
            'product_image'=>$filename,
            'product_description'=>$request->product_description,
            'status'=>$request->status,
        ]);

        if(isset($request->attribute)){
            ProductAttribute::where('product_id',$id)->delete();
            for($i=0;$i<count($request->attribute);$i++){
                 $att = new ProductAttribute;
                 $att->product_id = $id;
                 $att->att_type_id = $request->attribute[$i];
                 $att->save();
            }
         }

        return redirect()->route('product.index')->with('success','Product has been updated successfully.');
    }

}
