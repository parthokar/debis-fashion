<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use App\Models\Attribute;
use Illuminate\Http\Request;
use DataTables;

class ProductAttributeController extends Controller
{

      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Attribute::orderBy('id','desc')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        return '<a href="' . route('attribute.edit', $row->id) .'" class="edit btn btn-primary btn-sm">Edit</a>';
                     })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('dashboard.admin.product.attribute.index');
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {

        return view('dashboard.admin.product.attribute.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $request->validate([
            'type' => 'required|unique:attributes,type',
        ]);

        Attribute::create($request->post());

        return redirect()->route('attribute.index')->with('success','Attribute has been created successfully.');
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\CourseCategory  $company
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {

        $attribute=Attribute::find($id);
        return view('dashboard.admin.product.attribute.edit',compact('attribute'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\company  $company
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request,$id)
    {
        $attribute=Attribute::find($id);
        $request->validate([
            'type' => 'required|unique:attributes,type,'.$attribute->id
        ]);

        $attribute->fill($request->post())->save();

        return redirect()->route('attribute.index')->with('success','Attribute Has Been updated successfully');
    }
}
