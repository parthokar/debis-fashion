<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Offer;
use DataTables;

class OfferController extends Controller
{

      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Offer::orderBy('id','desc')->get();
            return Datatables::of($data)
                    ->addIndexColumn()
                    ->addColumn('status', function($row){
                         if($row->status==1){
                           return 'Active';
                         }else{
                            return 'In active';
                         }
                    })
                    ->addColumn('image', function($row){
                        return '<img src="'.asset('dashboard/offer/'.$row->image).'" width="30%" height="30%" class="img-fluid">';
                     })
                    ->addColumn('action', function($row){
                        return '<a href="' . route('offer.edit', $row->id) .'" class="edit btn btn-primary btn-sm">Edit</a>';
                     })
                    ->rawColumns(['action','image'])
                    ->make(true);
        }
        return view('dashboard.admin.offer.index');
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('dashboard.admin.offer.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $request->validate([
            'offer_title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

          // Get the file from the request
          $image = $request->file('image');

          // Generate a unique name for the file to avoid conflicts
          $filename = time().'.'.$image->getClientOriginalExtension();

          // Save the image to the public folder
          $image->move('dashboard/offer/', $filename);

          Offer::create([
                'offer_title'=>$request->offer_title,
                'image'=>  $filename,
                'status'=>$request->status,
          ]);

          return redirect()->route('offer.index')->with('success','Offer has been created successfully.');
      }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\CourseCategory  $company
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {

        $category=Offer::find($id);
        return view('dashboard.admin.offer.edit',compact('category'));
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\company  $company
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request,$id)
    {
        $category=Offer::find($id);
        $request->validate([
            'offer_title' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->hasFile('image')) {
            // Get the file from the request
      $image = $request->file('image');

      // Generate a unique name for the file to avoid conflicts
      $filename = time().'.'.$image->getClientOriginalExtension();

      // Save the image to the public folder
      $image->move('dashboard/offer/', $filename);
      }
       else
       {
            $filename = $category->image;
       }

      Offer::where('id',$id)->update([
        'offer_title'=>$request->offer_title,
        'image'=>  $filename,
        'status'=>$request->status,
      ]);

        return redirect()->route('offer.index')->with('success','Offer Has Been updated successfully');
    }

}
