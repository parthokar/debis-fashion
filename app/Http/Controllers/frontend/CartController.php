<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Support\Facades\Session;


class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = cartItem();
        return view('frontend.cart',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $product = Product::find($request->product_id);

         $product_id = $request->product_id;

         $attribute_id = $request->attribute_id;

        if($product_id &&  $attribute_id){

            $old_data = Cart::where(['product_id'=>$product_id,'attribute_id'=>$attribute_id,'customer_id'=>Session::getId()])->first();

            if($old_data){
                $total_qty   = $request->quantity+$old_data->quantity;
                $total_price = $product->product_price*$total_qty;
                $data = [
                    'product_id'=>$product_id,
                    'attribute_id'=>$attribute_id,
                    'customer_id'=>Session::getId(),
                    'quantity'=>$request->quantity+$old_data->quantity,
                    'total_price'=>$total_price,
                ];
                Cart::where(['product_id'=>$product_id,
                'attribute_id'=>$attribute_id,
                'customer_id'=>Session::getId()])->update($data);
            }
            else{
                $total_qty = $request->quantity;
                $total_price = $product->product_price*$total_qty;
                $data = [
                    'product_id'=>$product_id,
                    'attribute_id'=>$attribute_id,
                    'customer_id'=>Session::getId(),
                    'quantity'=>$request->quantity,
                    'total_price'=>$total_price,
                ];
                Cart::insert($data);
            }

        }
        elseif($product_id &&  $attribute_id==null){
            $old_data = Cart::where(['product_id'=>$product_id,'customer_id'=>Session::getId()])->first();
            if($old_data){
                $total_qty   = $request->quantity+$old_data->quantity;
                $total_price = $product->product_price*$total_qty;
                $data = [
                    'product_id'=>$product_id,
                    'attribute_id'=>$attribute_id,
                    'customer_id'=>Session::getId(),
                    'quantity'=>$total_qty,
                    'total_price'=>$total_price,
                ];
                Cart::where(['product_id'=>$product_id,
                'customer_id'=>Session::getId()])->update($data);
            }
            else{
                $total_qty = $request->quantity;
                $total_price = $product->product_price*$total_qty;
                $data = [
                    'product_id'=>$product_id,
                    'attribute_id'=>$attribute_id,
                    'customer_id'=>Session::getId(),
                    'quantity'=>$total_qty,
                    'total_price'=>$total_price,
                ];
                Cart::insert($data);
            }
        }

       return response()->json(['success','প্রোডাক্ট আপনার ব্যাগে যুক্ত করা হয়েছে']);

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if($request->quantity==0){
            Cart::find($id)->delete();
            return redirect()->route('cart.index')->with('success','আইটেম আপডেট করা হয়েছে');
        }

        $cart = Cart::find($id);

        $product = Product::find($cart->product_id);

        $total_price = $product->product_price*$request->quantity;

        $data = [
            'quantity'=>$request->quantity,
            'total_price'=>$total_price,
        ];

        Cart::where('id',$id)->update($data);

        return redirect()->route('cart.index')->with('success','আইটেম আপডেট করা হয়েছে');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cart = Cart::find($id);

        $cart->delete();
        return redirect()->route('cart.index')->with('error','আইটেম সরানো হয়েছে');
    }
}
