<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductAttribute;
use App\Models\Slider;
use App\Models\Offer;

class FrontendController extends Controller
{
    //home page
    public function index(){
        $data['category'] = ProductCategory::where('status',1)->orderBy('id','DESC')->get();
        $data['product']  = Product::where('status',1)->orderBy('id','DESC')->get();
        $data['slider']  = Slider::where('status',1)->get();
        $data['offer']  = Offer::where('status',1)->get();
        return view('frontend.index',compact('data'));
    }

     //shop page
     public function shop(Request $request){
        if($request->attribute_id){
            $data['product']  = Product::where('status',1)
            ->join('product_attributes','product_attributes.product_id','=','products.id')
            ->where('att_type_id',$request->attribute_id)
            ->simplePaginate(10);
        }else{
            $data['product']  = Product::where('status',1)->simplePaginate(10);
        }

        $data['size']     = allSize();
        return view('frontend.shop',compact('data'));
    }

    public function productDetails($id){

        $data['product'] = Product::where(['id'=>$id,'status'=>1])->first();

        $data['attribute'] = ProductAttribute::
        where('product_id',$data['product']->id)
        ->leftjoin('attributes','attributes.id','=','product_attributes.att_type_id')
        ->select('att_type_id','type')
        ->get();

        $data['category_product'] = Product::where(['category_id'=>$data['product']->category_id,'status'=>1])->get();

        return response()->json(['data' => $data]);

    }

    public function categoryProduct($id){

        $data['product'] = Product::with('attribute')->where(['category_id'=>$id,'status'=>1])->orderBy('id','DESC')->get();

        return view('frontend.category-product',compact('data'));
    }

    public function searchProduct(Request $request){

        $data['product'] = Product::with('attribute')->where('product_title', 'LIKE', '%' . $request->search . '%')->where('status',1)->orderBy('id','DESC')->get();

        return view('frontend.search-product',compact('data'));
    }
}
