<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Support\Facades\Session;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

class CheckoutController extends Controller
{
    public function index(){
        $data = cartItem();
        return view('frontend.checkout',compact('data'));
    }

    function sendFacebookPageMessage($pageAccessToken, $pageId, $message) {
        $fb = new Facebook([
            'app_id' => 'your_app_id',
            'app_secret' => 'your_app_secret',
            'default_graph_version' => 'v13.0', // Update to the latest version
        ]);

        try {
            // Send a message to your page owner
            $response = $fb->post("/$pageId/messages", [
                'message' => $message,
            ], $pageAccessToken);

            $graphNode = $response->getGraphNode();
            // Handle success

        } catch (FacebookResponseException $e) {
            // Handle error

        } catch (FacebookSDKException $e) {
            // Handle error
        }
    }

    public function placeOrder(Request $request){

        $data = cartItem();

        $order = new Order;
        $order->customer_id = Session::getId();
        $order->name = $request->name;
        $order->email = $request->email;
        $order->mobile = $request->mobile;
        $order->address = $request->address;
        $order->delivery_charge = str_replace("Tk ", "", deliveryCharge());
        $order->save();

        $invoice = $order->id.rand();

        Order::where('id',$order->id)->update(['invoice_no'=>$invoice]);

        foreach($data as $item){
            $order_item = new OrderItem;
            $order_item->order_id = $order->id;
            $order_item->product_id = $item->product_id;
            $order_item->attribute_id = $item->attribute_id;
            $order_item->quantity = $item->quantity;
            $order_item->total_price = $item->total_price;
            $order_item->save();
        }

        $this->sendFacebookPageMessage();

        cartDelete();

        return redirect()->route('checkout_route')->with('success', "ধন্যবাদ অর্ডার টি সফলভাবে সম্পন্ন হয়েছে.\nআপনার অর্ডার নাম্বার " .  $invoice . "।\nএই অর্ডার নাম্বার দিয়ে আপনি ট্র্যাক করতে পারবেন");


    }
}
