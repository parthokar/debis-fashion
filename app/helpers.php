<?php

use App\Models\Cart;
use App\Models\Attribute;
use Illuminate\Support\Facades\Session;

function cartItem(){

   return Cart::with('products')
   ->where('customer_id',Session::getId())
   ->leftjoin('attributes','attributes.id','=','carts.attribute_id')
   ->select('attributes.type','carts.*')
   ->get();
}

function cartCount(){
    if(Auth::check()){
        $id=auth()->user()->id;
      }else{
        $id=0;
      }
   return Cart::where('customer_id',$id)->count();
}

function subTotal(){
    $amount = Cart::where('customer_id',Session::getId())->sum('total_price');
    return "টাকা ". number_format($amount);
}

function deliveryCharge(){
        return "টাকা ". 150;

}

function adminDeliveryCharge(){

    return 150;
}

function total(){
    $amount = Cart::where('customer_id',Session::getId())->sum('total_price')+150;
    if($amount>0){
        return "টাকা ". number_format($amount);
    }else{
        return "টাকা ". 0;
    }

}

function cartDelete(){
    return Cart::where('customer_id',Session::getId())->delete();
}

function allSize(){
  return Attribute::orderBy('id','ASC')->get();
}

function englishToBanglaNumber($number) {
    $banglaNumbers = array(
        '0' => '০',
        '1' => '১',
        '2' => '২',
        '3' => '৩',
        '4' => '৪',
        '5' => '৫',
        '6' => '৬',
        '7' => '৭',
        '8' => '৮',
        '9' => '৯',
    );

    // Convert the number to a string
    $numberStr = (string)$number;

    // Replace each digit with its Bengali counterpart
    $banglaNumber = '';
    for ($i = 0; $i < strlen($numberStr); $i++) {
        $digit = $numberStr[$i];
        if (array_key_exists($digit, $banglaNumbers)) {
            $banglaNumber .= $banglaNumbers[$digit];
        } else {
            // If the character is not a digit, keep it as it is
            $banglaNumber .= $digit;
        }
    }

    return $banglaNumber;
}


