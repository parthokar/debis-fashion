<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\frontend\FrontendController;
use App\Http\Controllers\admin\ProductCategoryController;
use App\Http\Controllers\admin\ProductAttributeController;
use App\Http\Controllers\admin\ProductController;
use App\Http\Controllers\admin\OfferController;
use App\Http\Controllers\admin\SliderController;
use App\Http\Controllers\frontend\CartController;
use App\Http\Controllers\frontend\CheckoutController;
use App\Http\Controllers\admin\OrderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', [FrontendController::class, 'index'])->name('home_route');
Route::get('/shop', [FrontendController::class, 'shop'])->name('shop_route');
Route::get('/product-details/{id}', [FrontendController::class, 'productDetails'])->name('product_details_route');
Route::get('/category-product/{id}', [FrontendController::class, 'categoryProduct'])->name('category_product_route');
Route::get('/search', [FrontendController::class, 'searchProduct'])->name('search_product_route');

Route::resource('cart',CartController::class);
Route::get('checkout',[CheckoutController::class,'index'])->name('checkout_route');
Route::post('place-order',[CheckoutController::class,'placeOrder'])->name('place_order_route');


//admin route
Route::prefix('admin')->group(function () {
    Route::group(['middleware' => 'auth'], function(){
        Route::get('dashboard',[AdminController::class, 'index'])->name('admin.dashboard');
        Route::resource('category',ProductCategoryController::class);
        Route::resource('attribute',ProductAttributeController::class);
        Route::resource('product',ProductController::class);
        Route::resource('offer',OfferController::class);
        Route::resource('slider',SliderController::class);
        Route::resource('order',OrderController::class);
    });
});





