@extends('layouts.app')
@section('title')
    Product
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Dashboard</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Order Item</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <section class="content">
            <div class="container-fluid">


                <div class="container mt-5">
                    <h2>Customer Information</h2>
                    <!-- Customer Information Section -->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Customer Name</h5>
                                    <p class="card-text">{{ $data->name }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Mobile</h5>
                                    <p class="card-text">{{ $data->mobile }}</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Address</h5>
                                    <p class="card-text">{{ $data->address }}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Product List Section -->
                    <div class="row mt-4">
                        <div class="col-md-12">
                            <h4>Order List</h4>
                            <h4>Total Price: {{ number_format($total) }} + Delivery charge {{ adminDeliveryCharge() }} =
                                {{ number_format($total + adminDeliveryCharge()) }}</h4>
                            <table class="table table-bordered" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>Sl</th>
                                        <th>Image</th>
                                        <th>Product Name</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Sample product row -->
                                    @foreach ($dataItem as $key => $item)
                                        <tr>
                                            <td>{{ ++$key }}</td>
                                            <td><img width="100px"
                                                    src="{{ asset('dashboard/product/' . $item->product_image) }}"></td>
                                            <td>{{ $item->product_title }} {{ $item->type }}</td>
                                            <td>{{ $item->quantity }}</td>
                                            <td>{{ $item->product_price }}</td>
                                            <td>{{ $item->total_price }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>








            </div>
        </section>
    </div>
@endsection
