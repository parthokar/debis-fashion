@extends('layouts.app')
@section('title') Teacher @endsection
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Product Create</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <div class="card">
            @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="row container">
                    <h4 class="mt-3 mb-3">Product Create</h4>
                    <form action="{{ route('product.update',$data['product']->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                         {{method_field('PATCH')}}

                         <div class="row container">
                            <div class="col-xs-12 col-sm-12 col-md-4 mb-2">
                                <div class="form-group">
                                    <strong>Category: <span style="color:red">*</span></strong>
                                    <select class="form-control" name="category_id" required>
                                        @foreach($data['category'] as $c)
                                         <option value="{{$c->id}}" @if($c->id==$data['product']->category_id) selected @endif>{{$c->product_category}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-4 mb-2">
                                <div class="form-group">
                                    <strong>Product title: <span style="color:red">*</span></strong>
                                    <input type="text" name="product_title" class="form-control" value="{{$data['product']->product_title}}" placeholder="Enter title" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 mb-2">
                                <div class="form-group">
                                    <strong>Product price: <span style="color:red">*</span></strong>
                                    <input type="text" name="product_price" class="form-control" value="{{$data['product']->product_price}}" placeholder="Enter course price" required>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-3 mb-2">
                                <div class="form-group">
                                    <strong>Discount price:</strong>
                                    <input type="text" name="regular_price" value="{{$data['product']->regular_price}}" class="form-control"
                                        placeholder="Enter course price">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-4 mb-2">
                                <img src="{{asset('dashboard/product/'.$data['product']->product_image)}}" width="50%" height="50%" class="img-fluid"><br>
                                <div class="form-group">
                                    <strong>Product image: <span style="color:red">*</span></strong>
                                    <input type="file" name="product_image" class="form-control">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 mb-2">
                                <div class="form-group mt-3">
                                    <strong>Attribute:</strong><br>
                                    @foreach($data['att_type'] as $att)
                                        @php
                                                $atts = DB::table('product_attributes')
                                                ->where('att_type_id',$att->id)
                                                ->where('product_id',$data['product']->id)
                                                ->select('att_type_id')->first();
                                        @endphp
                                        <input type="checkbox" @if(isset($atts->att_type_id)==$att->id) checked @endif id="{{$att->id}}" name="attribute[]" value="{{$att->id}}">
                                        <label for="{{$att->id}}"> {{$att->type}}</label><br>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 mb-2">
                                <div class="form-group mt-3">
                                    <strong>Status:</strong>
                                    <select class="form-control" name="status" required>
                                        <option value="1" @if($data['product']->status==1) selected @endif>Active</option>
                                        <option value="0" @if($data['product']->status==0) selected @endif>InActive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 mb-2">
                                <div class="form-group">
                                    <strong>Product Description: <span style="color:red">*</span></strong>
                                    <textarea id="editor" name="product_description" class="form-control" required>{{$data['product']->product_description}}</textarea>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 mt-3 mb-3">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
          </div>
      </div>
    </section>
  </div>
  @section('script')
  <script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
  <script>
      CKEDITOR.replace('editor');
  </script>
@endsection
@endsection


