@extends('layouts.frontend.app')
@section('title') হোম @endsection
@section('content')
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="#">হোম</a>
                    <a class="breadcrumb-item text-dark" href="#">প্রোডাক্ট</a>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->


    <!-- Shop Detail Start -->
    <div class="container-fluid pb-5">
        <div class="row px-xl-5">
            <div class="col-lg-5 mb-30">
                <div id="product-carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner bg-light">
                        <div class="carousel-item active">
                            <img class="w-100 h-100" src="{{asset('dashboard/product/'.$data['product']->product_image)}}" alt="Image">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#product-carousel" data-slide="prev">
                        <i class="fa fa-2x fa-angle-left text-dark"></i>
                    </a>
                    <a class="carousel-control-next" href="#product-carousel" data-slide="next">
                        <i class="fa fa-2x fa-angle-right text-dark"></i>
                    </a>
                </div>
            </div>


            <div class="col-lg-7 h-auto mb-30">
                @if (Session::get('success'))
                  <div class="alert alert-info">
                     {{Session::get('success')}}
                  </div>
                @endif
                <form method="post" action="{{route('cart.store')}}">
                    @csrf
                    <input type="hidden" name="product_id" value="{{$data['product']->id}}">
                <div class="h-100 bg-light p-30">
                    <h3>{{$data['product']->product_title}}</h3>
                    <h3 class="font-weight-semi-bold mb-4">
                        @if($data['product']->regular_price>0)
                         TK  {{$data['product']->regular_price}}<del>TK {{$data['product']->product_price}}</del>
                        @else
                         TK  {{$data['product']->product_price}}
                        @endif
                    </h3>
                    <div class="d-flex mb-3">
                            @foreach($data['product']->attribute as $att)
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" class="custom-control-input" id="{{$att->id}}" name="attribute_id" value="{{$att->id}}">
                                    <label class="custom-control-label" for="{{$att->id}}">{{optional($att->attributeDetails)->type}}</label>
                                </div>
                            @endforeach
                    </div>
                    <div class="d-flex align-items-center mb-4 pt-2">
                        <div class="input-group quantity mr-3" style="width: 130px;">
                            <div class="input-group-btn">
                                <button class="btn btn-primary btn-minus">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <input type="text" class="form-control bg-secondary border-0 text-center" name="quantity" value="1">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-plus">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <button class="btn btn-primary px-3"><i class="fa fa-shopping-cart mr-1"></i>
                            ব্যাগে রাখো
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>

        
        <div class="row px-xl-5">
            <div class="col">
                <div class="bg-light p-30">
                    <div class="nav nav-tabs mb-4">
                        <a class="nav-item nav-link text-dark active" data-toggle="tab" href="#tab-pane-1">প্রোডাক্ট সম্পর্কে</a>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="tab-pane-1">
                            <p>{{$data['product']->product_description}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Shop Detail End -->


    <!-- Products Start -->
    <div class="container-fluid py-5">
        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">
            আরও প্রোডাক্ট</span></h2>
        <div class="row px-xl-5">
            <div class="col">
                <div class="owl-carousel related-carousel">
                    @foreach($data['category_product'] as $item)
                    <div class="product-item bg-light">
                        <div class="product-img position-relative overflow-hidden">
                            <img class="img-fluid w-100" src="{{asset('dashboard/product/'.$item->product_image)}}" alt="">
                            <div class="product-action">
                                <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-sync-alt"></i></a>
                                <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-search"></i></a>
                            </div>
                        </div>
                        <div class="text-center py-4">
                            <a class="h6 text-decoration-none text-truncate" href="{{route('product_details_route',$item->id)}}">{{$item->product_title}}</a>
                            <div class="d-flex align-items-center justify-content-center mt-2">
                                @if($item->regular_price>0)
                                <h5>TK  {{$item->regular_price}}</h5><h6 class="text-muted ml-2"><del>TK {{$item->product_price}}</del> </h6>
                                @else
                                <h5>TK  {{$item->product_price}}</h5><h6 class="text-muted ml-2"></h6>
                              @endif
                            </div>
                            <div class="d-flex align-items-center justify-content-center mb-1">
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- Products End -->
@endsection
