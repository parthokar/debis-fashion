@extends('layouts.frontend.app')
@section('title')
    হোম
@endsection
@section('content')
    <!-- Carousel Start -->
    <div class="container-fluid mb-3">
        <div class="row px-xl-5">
            <div class="col-lg-8">
                <div id="header-carousel" class="carousel slide carousel-fade mb-30 mb-lg-0" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#header-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#header-carousel" data-slide-to="1"></li>
                        <li data-target="#header-carousel" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        @foreach ($data['slider'] as $key => $carousel)
                            <div class="carousel-item position-relative {{ $loop->first ? 'active' : '' }}"
                                style="height: 430px;">
                                <img class="position-absolute w-100 h-100"
                                    src="{{ asset('dashboard/slider/' . $carousel->image) }}" style="object-fit: cover;">
                                <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                    <div class="p-3" style="max-width: 700px;">
                                        <h1 class="display-4 text-white mb-3 animate__animated animate__fadeInDown">
                                            {{ $carousel->slider_title }}</h1>
                                        <p class="mx-md-5 px-5 animate__animated animate__bounceIn"></p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                @foreach ($data['offer'] as $key => $offers)
                    <div class="product-offer mb-30" style="height: 200px;">
                        <img class="img-fluid" src="{{ asset('dashboard/offer/' . $offers->image) }}" alt="">
                        <div class="offer-text">
                            <h6 class="text-white text-uppercase">{{ $offers->offer_title }}</h6>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- Carousel End -->


    <!-- Featured Start -->
    <div class="container-fluid pt-5">
        <div class="row px-xl-5 pb-3">
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="d-flex align-items-center bg-light mb-4" style="padding: 30px;">
                    <h1 class="fa fa-check text-primary m-0 mr-3"></h1>
                    <h5 class="font-weight-semi-bold m-0">মানসম্মত প্রোডাক্ট</h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="d-flex align-items-center bg-light mb-4" style="padding: 30px;">
                    <h1 class="fa fa-shipping-fast text-primary m-0 mr-2"></h1>
                    <h5 class="font-weight-semi-bold m-0">পরিবহন</h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="d-flex align-items-center bg-light mb-4" style="padding: 30px;">
                    <h1 class="fas fa-exchange-alt text-primary m-0 mr-3"></h1>
                    <h5 class="font-weight-semi-bold m-0">রিটার্ন পলিছি</h5>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                <div class="d-flex align-items-center bg-light mb-4" style="padding: 30px;">
                    <h1 class="fa fa-phone-volume text-primary m-0 mr-3"></h1>
                    <h5 class="font-weight-semi-bold m-0">২৪/৭ সাপোর্ট</h5>
                </div>
            </div>
        </div>
    </div>
    <!-- Featured End -->


    <!-- Categories Start -->
    <div class="container-fluid pt-5">
        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span
                class="bg-secondary pr-3">ক্যাটাগরি</span></h2>
        <div class="row px-xl-5 pb-3">
            @foreach ($data['category'] as $categorys)
                <div class="col-lg-3 col-md-4 col-sm-6 pb-1">
                    <a class="text-decoration-none" href="{{ route('category_product_route', $categorys->id) }}">
                        <div class="cat-item d-flex align-items-center mb-4">
                            <div class="overflow-hidden" style="width: 100px; height: 100px;">
                                <img class="img-fluid" src="{{ asset('dashboard/category/' . $categorys->image) }}"
                                    alt="">
                            </div>
                            <div class="flex-fill pl-3">
                                <h6>{{ $categorys->product_category }}</h6>
                                <small class="text-body">{{ $categorys->productCount($categorys->id) }} পণ্য</small>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
    <!-- Categories End -->

    <!-- Products Start -->
    <div class="container-fluid pt-5 pb-3">
        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span
                class="bg-secondary pr-3">প্রোডাক্ট</span></h2>
        <div class="row px-xl-5">
            @foreach ($data['product'] as $item)
                <div class="col-lg-3 col-md-4 col-sm-6 pb-1">
                    <div class="product-item bg-light mb-4">
                        <div class="product-img position-relative overflow-hidden">
                            <img class="fixed-height-image img-fluid w-100" src="{{ asset('dashboard/product/' . $item->product_image) }}"
                                alt="">

                        </div>
                        <div class="text-center">
                            <div class="h6 text-decoration-none text-truncate mt-3">
                                {{ $item->product_title }}</div>
                            <div class="d-flex align-items-center justify-content-center mt-2">
                                @if ($item->regular_price > 0)
                                    <h5>টাকা {{ englishToBanglaNumber($item->regular_price)  }}</h5>
                                    <h6 class="text-muted ml-2"><del>টাকা  {{ englishToBanglaNumber($item->product_price) }}</del> </h6>
                                @else
                                    <h5>টাকা {{ englishToBanglaNumber($item->product_price)  }}</h5>
                                    <h6 class="text-muted ml-2"></h6>
                                @endif
                            </div>
                            <div class="mb-5">
                                {{-- <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3">Payment</span></h5> --}}
                                <div class="bg-light">
                                    <button class="btn btn-block btn-primary font-weight-bold py-3 open-modal-btn"
                                        data-product-id="{{ $item->id }}">বিস্তারিত দেখুন</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <!-- Products End -->


    <!-- Bootstrap Modal -->
    <div class="modal" id="myModal" tabindex="-1" role="dialog" style="width:100%">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">প্রোডাক্ট</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="modalContent">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Bootstrap Modal -->


@section('script')
    <!-- Include FontAwesome CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Your modal button to trigger the modal -->
    <button class="open-modal-btn" data-product-id="1">Open Modal</button>

    <!-- Your modal container -->
    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content" id="modalContent">
                <!-- Modal content will be dynamically added here -->
            </div>
        </div>
    </div>

    <script>

        function changeQuantity(change) {
            const quantityInput = document.getElementById('quantity');
            let quantity = parseInt(quantityInput.value);
            quantity += change;
            if (quantity < 1) {
                quantity = 1;
            }
            quantityInput.value = quantity;
        }

        function addToCart(productid) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            const attributeRadio = document.querySelector('input[name="attribute_id"]:checked');
            const attribute_id = attributeRadio ? attributeRadio.value : null;

            const quantityInput = document.getElementById('quantity');
            const quantity = quantityInput ? parseInt(quantityInput.value) : 1;


            if (!attribute_id) {
                // If no attribute is selected, show an error message
                Swal.fire({
                    icon: 'error',
                    text: 'কার্টে যোগ করার আগে অনুগ্রহ করে একটি সিলেক্ট করুন',
                    showConfirmButton: true,
                });
                return; // Return early and do not proceed with adding to cart
            }

            $.ajax({
                type: 'POST',
                url: '{{ route('cart.store') }}',
                data: {
                    product_id: productid,
                    attribute_id: attribute_id,
                    quantity: quantity
                },
                success: function(response) {
                    Swal.fire({
                        icon: 'success',
                        title: response[1],
                        showConfirmButton: false,
                        showCloseButton: true,
                        showCloseButton: true,
                        footer: '<a class="btn btn-primary" href="{{ route('cart.index') }}">আপনার অর্ডার দেখোন</a><button type="button" class="ml-3 btn btn-secondary" data-dismiss="modal">X</button>',
                        allowOutsideClick: false,
                        timer: 8000
                    });
                },
                error: function(error) {
                    console.log(error);
                }
            });
        }

        // Function to generate attribute radios HTML
        function generateAttributeRadios(attributes) {
            var radiosHtml = '';
            attributes.forEach(function(attribute) {
                radiosHtml += `
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="attribute_${attribute.att_type_id}" name="attribute_id" value="${attribute.att_type_id}">
                    <label class="custom-control-label" for="attribute_${attribute.att_type_id}">${attribute.type}</label>
                </div>
            `;
            });
            return radiosHtml;
        }

        // Function to open the modal and fetch data using AJAX
        function openModal(productId) {
            $.ajax({
                type: 'GET',
                url: '/product-details/' + productId,
                dataType: 'json',
                success: function(response) {
                    var product = response.data.product;
                    attributes = response.data.attribute; // Assign the 'attribute' to the global variable
                    var modalContent = `
                    <div class="modal-header">
                        <h5 class="modal-title" id="productModalLabel">${product.product_title}</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <!-- Product Image -->
                                <img src="{{ asset('dashboard/product/') }}/${product.product_image}" alt="${product.product_title}" class="fixed-height-image img-fluid mb-3">
                            </div>
                            <div class="col-md-6">
                                <!-- Price and Regular Price -->
                                <p>টাকা: <span id="productPrice">${product.regular_price ? + product.regular_price : product.product_price}  </span></p>
                                <p><del>${product.regular_price ? 'টাকা ' + product.product_price : ''}</del></p>
                                <!-- Product Description -->
                            </div>
                        </div>

                        <!-- Product Attributes -->
                        <div class="form-group mt-3" id="attributeContainer">
                            ${attributes && attributes.length >0 ? '<label>সিলেক্ট করুন</label> <br>' : ''}
                            ${attributes && Array.isArray(attributes) && attributes.length > 0 ? generateAttributeRadios(attributes) : ''}
                        </div>

                        <!-- Quantity input group -->
                        <label>পরিমাণ</label>
                        <div class="input-group quantity" style="width: 130px;">
                            <div class="input-group-btn">
                                <button class="btn btn-primary btn-minus" onclick="changeQuantity(-1)">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <input type="text" class="form-control bg-secondary border-0 text-center" name="quantity" id="quantity" value="1">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-primary btn-plus" onclick="changeQuantity(1)">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        ${product.product_description ?  product.product_description : ''}
                    </div>

                    <div class="modal-footer">
                        <!-- Close button -->
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">X</button>
                        <!-- Add to Cart button -->
                        <button type="button" class="btn btn-primary" onclick="addToCart(${product.id})">কার্টে যোগ করুন</button>
                    </div>
                `;

                    $('#modalContent').html(modalContent);
                    $('#myModal').modal('show');
                },
                error: function(error) {
                    console.error('Error fetching data: ' + JSON.stringify(error));
                }
            });
        }

        // Attach the function to the button click event using a class selector
        $(document).ready(function() {
            $('.open-modal-btn').click(function() {
                var productId = $(this).data('product-id');
                openModal(productId);
            });
        });
    </script>
@endsection
@endsection
