@extends('layouts.frontend.app')
@section('title') হোম @endsection
@section('content')

    <!-- Cart Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-lg-8 table-responsive mb-5">
                <table class="table table-light table-borderless table-hover text-center mb-0">
                    <thead class="thead-dark">
                        <tr>
                            <th>প্রোডাক্ট</th>
                            <th>টাকা</th>
                            <th>পরিমাণ</th>
                            <th>মোট</th>
                            <th>ডিলিট</th>
                        </tr>
                    </thead>
                    <tbody class="align-middle">
                        @if (Session::get('success'))
                        <div class="alert alert-info">
                             {{Session::get('success')}}
                          </div>
                        @endif
                        @if (Session::get('error'))
                        <div class="alert alert-danger">
                             {{Session::get('error')}}
                          </div>
                        @endif
                      @foreach($data as $item)
                            <tr>
                                <td class="align-middle"><img src="{{asset('dashboard/product/'.$item->products->product_image)}}" alt="" style="width: 50px;"> {{$item->products->product_title}} {{$item->type}}</td>
                                <td class="align-middle">{{englishToBanglaNumber($item->products->product_price)}}</td>
                                <form class="mb-30" action="{{route('cart.update',$item->id)}}" method="post">
                                    @csrf
                                    {{method_field('PATCH')}}
                                    <td class="align-middle">
                                        <div class="input-group quantity mx-auto" style="width: 100px;">
                                            <div class="input-group-btn">
                                                <button class="btn btn-sm btn-primary btn-minus" >
                                                <i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                            <input type="text" name="quantity" class="form-control form-control-sm bg-secondary border-0 text-center" value="{{$item->quantity}}">
                                            <div class="input-group-btn">
                                                <button class="btn btn-sm btn-primary btn-plus">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </td>
                                  </form>
                                <td class="align-middle">{{englishToBanglaNumber(number_format($item->total_price))}}</td>
                                <form class="mb-30" action="{{route('cart.destroy',$item->id)}}" method="post">
                                    @csrf
                                    {{method_field('DELETE')}}
                                  <td class="align-middle"><button class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></td>
                                </form>
                            </tr>

                         @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-lg-4">
                {{-- <form class="mb-30" action="">
                    <div class="input-group">
                        <input type="text" class="form-control border-0 p-4" placeholder="Coupon Code">
                        <div class="input-group-append">
                            <button class="btn btn-primary">Apply Coupon</button>
                        </div>
                    </div>
                </form> --}}
                <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3">প্রোডাক্ট সারাংশ</span></h5>
                <div class="bg-light p-30 mb-5">
                    <div class="border-bottom pb-2">
                        <div class="d-flex justify-content-between mb-3">
                            <h6>সাবটোটাল</h6>
                            <h6>{{englishToBanglaNumber(subTotal())}}</h6>
                        </div>
                        <div class="d-flex justify-content-between">
                            <h6 class="font-weight-medium">ডেলিভারি চার্জ</h6>
                            <h6 class="font-weight-medium">{{englishToBanglaNumber(deliveryCharge())}}</h6>
                        </div>
                    </div>
                    <div class="pt-2">
                        <div class="d-flex justify-content-between mt-2">
                            <h5>সর্বমোট</h5>
                            <h5>{{englishToBanglaNumber(total())}}</h5>
                        </div>
                        <a href="{{route('checkout_route')}}" class="btn btn-block btn-primary font-weight-bold my-3 py-3">অর্ডার করতে এগিয়ে যান</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Cart End -->
@endsection
