@extends('layouts.frontend.app')
@section('title') প্রোডাক্ট @endsection
@section('content')
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="#">হোম</a>
                    <a class="breadcrumb-item text-dark" href="#">প্রোডাক্ট</a>
                    <span class="breadcrumb-item active">প্রোডাক্ট লিস্ট</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->


    <!-- Shop Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <!-- Shop Sidebar Start -->

            <div class="col-lg-3 col-md-4">
                <form action="{{ route('shop_route') }}" method="GET">
                <!-- Size Start -->
                <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3">সাইজ অনুযায়ী সার্চ করুন</span></h5>
                <div class="bg-light p-4 mb-30">
                    <button class="btn btn-primary px-3"><i class="fa fa-search mr-1"></i>
                        সার্চ করুন
                    </button>
                        @foreach($data['size'] as $sizes)
                            <div class="custom-control d-flex align-items-center justify-content-between mb-3">
                                <input onclick="radioChanged({{$sizes->id}})" type="radio" class="custom-control-input" id="{{$sizes->id}}" name="attribute_id" value="{{$sizes->id}}" {{ $sizes->selected ? 'checked' : '' }}">
                                <label class="custom-control-label" for="{{$sizes->id}}"></label>
                                <span class="badge border font-weight-normal">{{$sizes->type}}</span>
                            </div>
                        @endforeach
                </div>
                <!-- Size End -->
            </form>
            </div>

            <!-- Shop Sidebar End -->


            <!-- Shop Product Start -->
            <div class="col-lg-9 col-md-8">
                <div class="row pb-3">
                    <div class="col-12 pb-1">
                        <div class="d-flex align-items-center justify-content-between mb-4">
                            <div>
                                {{$data['product']->links()}}
                            </div>

                        </div>
                    </div>
                    @foreach($data['product'] as $item)
                    <div class="col-lg-4 col-md-6 col-sm-6 pb-1">
                        <div class="product-item bg-light mb-4">
                            <div class="product-img position-relative overflow-hidden">
                                <img class="img-fluid w-100" src="{{asset('dashboard/product/'.$item->product_image)}}" alt="">
                                <div class="product-action">
                                    <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-shopping-cart"></i></a>
                                    <a class="btn btn-outline-dark btn-square" href=""><i class="far fa-heart"></i></a>
                                    <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-sync-alt"></i></a>
                                    <a class="btn btn-outline-dark btn-square" href=""><i class="fa fa-search"></i></a>
                                </div>
                            </div>
                            <div class="text-center py-4">
                                <a class="h6 text-decoration-none text-truncate" href="{{route('product_details_route',$item->id)}}">{{$item->product_title}}</a>
                                <div class="d-flex align-items-center justify-content-center mt-2">
                                    @if($item->regular_price>0)
                                    <h5>TK  {{$item->regular_price}}</h5><h6 class="text-muted ml-2"><del>TK {{$item->product_price}}</del> </h6>
                                    @else
                                    <h5>TK  {{$item->product_price}}</h5><h6 class="text-muted ml-2"></h6>
                                    @endif
                                </div>
                                <div class="d-flex align-items-center justify-content-center mb-1">
                                    <small class="fa fa-star text-primary mr-1"></small>
                                    <small class="fa fa-star text-primary mr-1"></small>
                                    <small class="fa fa-star text-primary mr-1"></small>
                                    <small class="fa fa-star text-primary mr-1"></small>
                                    <small class="fa fa-star text-primary mr-1"></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="col-12">
                        <nav>
                          <ul class="pagination justify-content-center">
                            {{$data['product']->links()}}
                          </ul>
                        </nav>
                    </div>

                </div>
            </div>
            <!-- Shop Product End -->
        </div>
    </div>
    <!-- Shop End -->
@endsection
