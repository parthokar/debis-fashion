@extends('layouts.frontend.app')
@section('title') হোম @endsection
@section('content')

    <!-- Checkout Start -->
    <div class="container-fluid">
        <form method="post" action="{{route('place_order_route')}}">
            @csrf
            <div class="row px-xl-5">

                <div class="col-lg-8">
                    @if(Session::get('success'))
                    <div class="alert alert-info text-center">
                       {{Session::get('success')}}
                    </div>
                    @endif
                    @if(count(cartItem())>0)
                    <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3">আপনার ইনফরমেশন</span></h5>
                    <div class="bg-light p-30 mb-5">
                        <div class="row">

                            <div class="col-md-6 form-group">
                                <label>নাম <span class="text-danger">*</span></label>
                                <input class="form-control" name="name" type="text" placeholder="নাম" required>
                            </div>

                            <div class="col-md-6 form-group">
                                <label>মোবাইল নাম্বার <span class="text-danger">*</span></label>
                                <input class="form-control" name="mobile" type="text" placeholder="+88" required>
                            </div>

                            <div class="col-md-12 form-group">
                                <label>ঠিকানা <span class="text-danger">*</span></label>
                                <textarea cols="10" rows="5" class="form-control" name="address" type="text" placeholder="ঠিকানা" required></textarea>
                            </div>

                        </div>
                    </div>
                    @endif
                </div>
                @if(count(cartItem())>0)
                <div class="col-lg-4">
                    <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3">মোট অর্ডার</span></h5>
                    <div class="bg-light p-30 mb-5">
                        <div class="border-bottom">
                            @foreach(cartItem() as $item)
                                <div class="d-flex justify-content-between">
                                    <p>{{$item->products->product_title}} => {{$item->type}} </p>
                                    <p>{{englishToBanglaNumber($item->products->product_price)}}*{{englishToBanglaNumber($item->quantity)}}</p>
                                </div>
                            @endforeach
                        </div>
                        <div class="border-bottom pt-3 pb-2">
                            <div class="d-flex justify-content-between mb-3">
                                <h6>সাবটোটাল</h6>
                                <h6>{{englishToBanglaNumber(subTotal())}}</h6>
                            </div>
                            <div class="d-flex justify-content-between">
                                <h6 class="font-weight-medium">ডেলিভারি চার্জ</h6>
                                <h6 class="font-weight-medium">{{englishToBanglaNumber(deliveryCharge())}}</h6>
                            </div>
                        </div>
                        <div class="pt-2">
                            <div class="d-flex justify-content-between mt-2">
                                <h5>সর্বমোট</h5>
                                <h5>{{englishToBanglaNumber(total())}}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="mb-5">
                        {{-- <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3">Payment</span></h5> --}}
                        <div class="bg-light">

                            <button class="btn btn-block btn-primary font-weight-bold py-3">অর্ডার করুন</button>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </form>
    </div>
    <!-- Checkout End -->
@endsection
