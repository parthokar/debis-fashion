<div class="container-fluid">
    <div class="row bg-secondary py-1 px-xl-5">
        <div class="col-lg-6 d-none d-lg-block">
            <div class="d-inline-flex align-items-center h-100">
                <a class="text-body mr-3" href="">অর্ডার ট্র্যাক </a>
            </div>
        </div>
        <div class="col-lg-6 text-center text-lg-right">
            <div class="d-inline-flex align-items-center">
                {{-- <div class="btn-group">
                    <a href="{{route('login')}}" class="btn btn-sm btn-light">অ্যাকাউন্ট</a>
                </div> --}}
                <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-light dropdown-toggle" data-toggle="dropdown">
                        বাংলা</button>
                </div>
            </div>
            <div class="d-inline-flex align-items-center d-block d-lg-none">
                <a href="" class="btn px-0 ml-2">
                    <i class="fas fa-heart text-dark"></i>
                    <span class="badge text-dark border border-dark rounded-circle" style="padding-bottom: 2px;">0</span>
                </a>
                <a href="" class="btn px-0 ml-2">
                    <i class="fas fa-shopping-cart text-dark"></i>
                    <span class="badge text-dark border border-dark rounded-circle" style="padding-bottom: 2px;">0</span>
                </a>
            </div>
        </div>
    </div>
    <div class="row align-items-center bg-light py-3 px-xl-5 d-none d-lg-flex">
        <div class="col-lg-4">
            <a href="{{route('home_route')}}" class="text-decoration-none">
                <span class="h1 text-uppercase text-primary bg-dark px-2">Debi's Fashion Hub</span>
            </a>
        </div>
        <div class="col-lg-4 col-6 text-left">
            <form action="{{route('search_product_route')}}" method="GET">
                <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="প্রোডাক্ট অনুসন্ধান করুন">
                    <div class="input-group-append">
                        <button class="input-group-text bg-transparent text-primary">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-4 col-6 text-right">
            <p class="m-0">গ্রাহক সেবা</p>
            <h5 class="m-0">+8801765456090</h5>
        </div>
    </div>
</div>
